# Autofwd
That's a simple web service that allows you to forward port in your
local network to someone knowing the password.

## TODO
* Add manual address insertion (admin only)
* Dockerize

## Issues
* Using current configs, page will load very slow with no visible reason.
  Seems that current configuration is fucked up somewhere

## How it works?
* It starts web server using [bottle](https://www.bottlepy.org) microframework
* When the user enters correct password on the page:
    * Session cookie is generated for the user
    * It made an attempt to connect to router via SSH and add forwarding rule
      for that user using iptables

## Requirements
* gunicorn
* nginx

## Installation
> Notice: In this example we use path `/srv/http/autofwd/`. 
  You can choose another path, but don't forget to edit it in config files 

1. Clone that repository in /srv/http/, so path to webserv.py will be /srv/http/autofwd/webserv.py
2. Create virtualenv: `/srv/http/autofwd $ virtualenv venv`
3. Activate it: 
   ```shell
   venv/bin/activate
   ```
4. Install requirements: 
   ```shell
   pip install -r requirements.txt
   ```
5. Edit .env file: 
   ```shell
   mv .env.template .env && vim .env
   ```
6. Install systemd unit file and edit it:
   ```shell
   # cp examples/systemd/autofwd.service /etc/systemd/system/
   # vim /etc/systemd/system/autofwd.service
   ```
7. Place nginx.conf to right location and edit it:
   ```shell
   cp examples/nginx.conf .
   vim nginx.conf
   ```
8. Reload systemd daemons, and good luck!
   ```shell
   systemctl daemon-reload
   systemctl start autofwd.service
   ```
