# coding=utf-8
import crypt
import logging
import os
import traceback
from datetime import datetime, timedelta
from functools import lru_cache
from ipaddress import ip_address

from bottle import (TEMPLATE_PATH, default_app, error, post, redirect, request,
                    response, route, run, static_file, template, view)
from paramiko import AutoAddPolicy, SSHClient, SSHException
from telegram import Bot
from telegram.ext import Updater

# Web server options
DEBUG = int(os.getenv("DEBUG", False))

LISTEN_ADDRESS = os.getenv("LISTEN_ADDRESS", "localhost")
LISTEN_PORT = int(os.getenv("LISTEN_PORT", 8080))

PASSWORD = os.getenv("PASSWORD", "12345")
SALT = os.getenv("SALT", crypt.mksalt())  # token salt
TOKEN_TTL = timedelta(days=30)  # token cookie expiration time

TEMPLATE_PATH += ["./templates/"]

# Router SSH connection options
ROUTER_HOST = os.getenv("ROUTER_HOST", "192.168.1.1")
ROUTER_SSH_PORT = int(os.getenv("ROUTER_SSH_PORT", 22))
ROUTER_SSH_USER = os.getenv("ROUTER_USER", "admin")
SSH_PRIVATE_KEY_PATH = os.getenv(
    "SSH_PRIVATE_KEY_PATH", os.getenv("HOME") + "/.ssh/id_rsa"
)

# Forwarding options
FORWARD_TO_ADDRESS = os.getenv("DESTINATION_ADDRESS", "192.168.1.246")
FORWARD_PORT = int(os.getenv("FORWARD_PORT", 22))
FORWARD_PROTO = os.getenv("FORWARD_PROTO", "tcp")

# Telegram options
TELEGRAM_BOT_TOKEN = os.getenv("TELEGRAM_BOT_TOKEN")
TELEGRAM_CHAT_ID = os.getenv("TELEGRAM_CHAT_ID")
TELEGRAM_PROXY_URL = os.getenv("TELEGRAM_PROXY_URL")
TELEGRAM_PROXY_USERNAME = os.getenv("TELEGRAM_PROXY_USERNAME")
TELEGRAM_PROXY_PASSWORD = os.getenv("TELEGRAM_PROXY_PASSWORD")


logger = logging.getLogger(__name__)
logging.basicConfig()


# region Router functions
def new_router_connection() -> SSHClient:
    """
    Connect to router via ssh
    """
    client = SSHClient()
    client.set_missing_host_key_policy(AutoAddPolicy())
    client.connect(
        hostname=ROUTER_HOST,
        port=ROUTER_SSH_PORT,
        username=ROUTER_SSH_USER,
        key_filename=SSH_PRIVATE_KEY_PATH,
    )
    return client


def router_have_rule(
    connection: SSHClient, src_ip, dst_ip, dst_port, proto="tcp"
) -> bool:
    """
    Check if router have specific port forwarding rule using iptables
    """
    check_lines = {
        f"-A vserver -s {src_ip}/32 -p {proto} -m {proto} --dport {dst_port} -j DNAT --to-destination {dst_ip}",
        f"-A FORWARD -s {src_ip}/32 -d {dst_ip}/32 -p {proto} -m {proto} --dport {dst_port} -j ACCEPT",
    }

    _stdin, stdout, _stderr = connection.exec_command("iptables-save")
    output = stdout.read().decode("utf-8")
    router_rules = set(output.split("\n"))

    return check_lines.issubset(router_rules)


def router_add_rule(connection: SSHClient, src_ip, dst_ip, dst_port, proto="tcp"):
    """
    Add port forwarding rule using iptables
    """
    commands = (
        f"iptables -t nat -I vserver 1 -s {src_ip} -p {proto} -m {proto} --dport {dst_port} -j DNAT --to-destination {dst_ip}",
        f"iptables -I FORWARD 7 -s {src_ip} -d {dst_ip} -p {proto} -m {proto} --dport {dst_port} -j ACCEPT",
    )

    for cmd in commands:
        _stdin, stdout, stderr = connection.exec_command(cmd)

        output = (stderr.read() + stdout.read()).decode("utf-8")
        if output:
            logger.error(f"Got output from iptables:\n # {cmd}\n{output}")
            raise RuntimeError


def router_have_vpn_client_connected(connection, client_ip):
    command = "cat /tmp/vpns.leases"
    _stdin, stdout, _stderr = connection.exec_command(command)
    output = stdout.read().decode("utf-8")

    return client_ip in output


# endregion


def telegram_send(message):
    if not TELEGRAM_BOT_TOKEN or not TELEGRAM_CHAT_ID:
        logger.warning(
            "Denied sending message via telegram: TELEGRAM_BOT_TOKEN or "
            "TELEGRAM_CHAT_ID variable is not set"
        )
        return

    @lru_cache()
    def get_bot() -> Bot:
        if not TELEGRAM_PROXY_URL:
            return Updater(TELEGRAM_BOT_TOKEN, use_context=True).bot

        request_kwargs = {
            "proxy_url": TELEGRAM_PROXY_URL,
            "urllib3_proxy_kwargs": {
                "username": TELEGRAM_PROXY_USERNAME,
                "password": TELEGRAM_PROXY_PASSWORD,
            },
        }

        if not TELEGRAM_PROXY_USERNAME:
            del request_kwargs["urllib3_proxy_kwargs"]

        updater = Updater(
            TELEGRAM_BOT_TOKEN, use_context=True, request_kwargs=request_kwargs
        )
        return updater.bot

    try:
        bot = get_bot()
        bot.send_message(TELEGRAM_CHAT_ID, message, parse_mode="markdown")
    except Exception:
        logger.error("Can't send telegram message", exc_info=True)


def get_user_ip():
    return request.get("REMOTE_ADDR") or request.get("HTTP_X_REAL_IP")


def verify_token(token):
    return token == crypt.crypt(PASSWORD + SALT, token)


def create_token():
    return crypt.crypt(PASSWORD + SALT)


def is_authorized():
    token = request.get_cookie("token")
    return verify_token(token)


@error(500)
@view("status")
def error_handler(e):
    tb_str = "".join(
        traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__)
    )

    logger.error(tb_str, extra={"path": request.path, "environ": request.environ})
    telegram_send(
        f"*autofwd:* HTTP 500\nRemote address: `{request.remote_addr}`\n"
        f"Path: `{request.path}`\nCookies: `{request.cookies.getunicode()}`\n"
        f"```\n{tb_str}\n```"
    )
    return {
        "ip": get_user_ip(),
        "success": False,
        "title": "Ошибка 500",
        "message": "Произошла критическая ошибка на сервере. Администратор оповещен",
    }


@error(404)
def page_404(*_args):
    response.status = 303
    response.headers['Location'] = '/'


@route("/")
def index_view(*_args):
    if is_authorized():
        return redirect("/status/")
    return template("login")


@route("/favicon.ico")
def favicon_view(*_args):
    return static_file("favicon.ico", root="static/")


@post("/api/login/")
def login_view():
    if request.json.get("password") != PASSWORD:
        response.status = 401
        return {"message": "Неправильный пароль"}

    response.set_cookie(
        "token", create_token(), path="/", expires=datetime.now() + TOKEN_TTL,
    )

    return {"message": "OK", "ip": get_user_ip()}


@route("/logout/")
def logout_view():
    response.set_cookie("token", "", path="/", expires=0)
    return redirect("/")


@route("/status/")
@view("status")
def status_view():
    if not is_authorized():
        return redirect("/")

    ip = get_user_ip()

    def success_message(title, text, **kwargs):
        return {
            "ip": ip,
            "success": True,
            "title": title,
            "message": text,
            **kwargs,
        }

    def error_message(title, text, **kwargs):
        return {
            "ip": ip,
            "success": False,
            "title": title,
            "message": text,
            **kwargs,
        }

    try:
        router = new_router_connection()
        server_connected = router_have_vpn_client_connected(router, FORWARD_TO_ADDRESS)

        if ip_address(ip).is_private and not DEBUG:
            return error_message(
                "Адрес не добавлен",
                "Вы пытаетесь добавить адрес локального сегмента сети",
                server_connected=server_connected,
            )

        if router_have_rule(
            router, ip, FORWARD_TO_ADDRESS, FORWARD_PORT, FORWARD_PROTO
        ):
            return success_message(
                "Доступ предоставлен",
                "Но адрес не был добавлен <br/>(<b>у вас уже есть доступ</b>)",
                server_connected=server_connected,
            )

        if DEBUG and request.params.get("iam") != "sure":
            return error_message(
                "Адрес не добавлен",
                "Мы в режиме отладки! Добавьте <b>решимости</b> к запросу: ?iam=sure",
                server_connected=server_connected,
            )
        router_add_rule(
            router,
            src_ip=ip,
            dst_ip=FORWARD_TO_ADDRESS,
            dst_port=FORWARD_PORT,
            proto=FORWARD_PROTO,
        )

        telegram_send(f"*autofwd:* Grant access to `{ip}`")

        return success_message(
            "Доступ предоставлен",
            "Теперь можно подключиться",
            server_connected=server_connected,
        )
    except (SSHException, RuntimeError, ValueError) as e:
        tb_str = "".join(
            traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__)
        )
        telegram_send(
            f"*autofwd:* Can't connect to router!\nRemote address: `{ip}`"
            f"```\n{tb_str}\n```"
        )
        return error_message(
            "Ошибка!",
            "Не удалось подключиться к роутеру!\n\nАдминистратор оповещен об инциденте",
        )


def main():
    logger.info(
        "{}: Listening connections on {}:{}".format(
            __file__, LISTEN_ADDRESS, LISTEN_PORT
        )
    )

    if DEBUG:
        logger.setLevel(logging.DEBUG)
        logger.info("Debug mode is ON")

    run(host=LISTEN_ADDRESS, port=LISTEN_PORT, debug=DEBUG, reloader=DEBUG)


if __name__ == "__main__":
    main()


app = application = default_app()  # WSGI Entry point (gunicorn needs that)
