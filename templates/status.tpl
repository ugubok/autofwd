<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{title or message}}</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-dark bg-dark justify-content-between">
    <div class="d-flex">
        <h4 class="text-info align-middle m-0 mr-4">Ваш IP: {{ip}}</h4>
    % if defined("server_connected"):
        % if server_connected:
        <h6 class="text-white p-1 align-middle m-0 bg-success rounded">Сервер в сети</h6>
        % else:
        <h6 class="text-white p-1 align-middle m-0 bg-danger rounded">Сервер оффлайн</h6>
        % end
    % end
    </div>
    <a class="btn btn-light" href="/logout/">Выйти</a>
</nav>

% if defined("server_connected") and not server_connected:
<div class="alert alert-danger" role="alert">
    Сервер не обнаружен в сети, возможно он выключен. После включения он должен появиться в сети в течение 1-3 минут
</div>
% end

<div class="container-sm mt-5">
    <div class="d-flex">
    % card_class = "info" if success else "danger"
        <div class="card border-{{card_class}} m-0 m-auto" style="max-width: 25rem;">
            <div class="card-body text-{{card_class}} justify-content-center">
            % if defined("title"):
                <h5 class="card-title">{{title}}</h5>
            % end
                {{!message}}
            </div>
        </div>
    </div>
</div>
</body>
</html>