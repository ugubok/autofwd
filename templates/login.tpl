<!DOCTYPE html>
<html>
<head>
    <title>Получить доступ</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <meta charset="UTF-8">
    <style>
        html,
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100%;
        }

        .form-signin {
            width: 100%;
            max-width: 330px;
            padding: 15px;
            margin: auto;
        }

        #error-message {
            width: 100%;
        }
    </style>
</head>
<body>


<form class="form-signin input-group" onsubmit="return formSubmit(this)">
    <div id="error-message" class="alert alert-danger" role="alert" hidden>
    </div>

    <input id="password" type="password" class="form-control" placeholder="Пароль" aria-label="Пароль"
           aria-describedby="button-addon2" required autofocus>
    <div class="input-group-append">
        <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Войти</button>
    </div>
</form>

<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
    function showError(msg) {
        const elErrorMessage = document.getElementById("error-message");
        elErrorMessage.innerHTML = msg;
        elErrorMessage.hidden = false;
    }

    function hideError() {
        const elErrorMessage = document.getElementById("error-message");
        elErrorMessage.hidden = true;
    }

    function formSubmit() {
        const elPassword = document.getElementById("password");

        hideError();

        axios.post("/api/login/", {password: elPassword.value}).then(function (e) {
            window.location = "/status/";
        }).catch(function (error) {
            if (error.response && error.response.data.message) {
                return showError(error.response.data.message);
            }
            showError(error);
        });

        return false;
    }
</script>

</body>
</html>